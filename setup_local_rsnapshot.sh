#!/bin/bash
# setup_local_rsnapshot.sh
# Éamonn Murray 2016
#
# This script generates an rsnapshot configuration to a backup a specified
# directory to an attached external drive, along with the associated crontab
# file which may be applied in the script.
#
# It takes one optional argument, which is the name of the directory to be
# backed up. If no directory is specified it will test if "/workspace/$USER"
# exists (these are local user storage directories on our systems) and back
# up this directory if so, if not then it will back up the users home
# directory.
#
# Note: The way this code decides which disks to list as external drives is
# very basic, but suits our user systems. It assumes that the internal drive
# is sda, and lists all other drives sdX. This may not be what you want if
# you're running this on your own system. In this case you may wish to modify
# the section beginning "disks=" at line numbers 98 and 99.
#
# Usage:
#   ./setup_local_rsnapshot.sh
# or
#   ./setup_local_rsnapshot.sh some/directory/to/backup
#
# Dependencies:
# rsnapshot (which depends on rsync), findmnt

scriptname=$(basename $0)

function echoerr { cat <<< "$@" 1>&2; }

function test_bin_in_path {
  local bin=$1
  local binpath=$(whereis -b $bin | awk '{print $2}')
  if [ -z $binpath ]; then
    echoerr "${scriptname} Error: $bin not found."
    echoerr "Exiting."
    exit 1
  fi
  echo $binpath
}

# Test necessary binaries are in the path and store their full path if so.
# These will be used to set the full paths to the exectutables in the
# rsnapshot.conf file and the crontab file.
rsnapshot_bin=$(test_bin_in_path rsnapshot)
rsync_bin=$(test_bin_in_path rsync)
cp_bin=$(test_bin_in_path cp)
du_bin=$(test_bin_in_path du)
findmnt_bin=$(test_bin_in_path findmnt)
#rsnapshotr-diff is optional
rdiff_bin=$(whereis -b rsnapshot-diff | awk '{print $2}')
if [ -z $rdiff_bin ]; then
  rdiff_line="#cmd_rsnapshot_diff	/usr/bin/rsnapshot-diff"
else
  rdiff_line="cmd_rsnapshot_diff	${rdiff_bin}"
fi

# This is the directory that will be backed up.
sourcedir="/workspace/${USER}/"

# Test this default sourcedir exists, if not, then reset the default to the
# user home directory.
if [ ! -d "$sourcedir" ]; then
  sourcedir="$(eval echo "~")"
fi

# Now if an argument has been passed to the script, assume this is the
# directory to back up.
if [ $# -gt 1 ]; then
  echoerr "Usage: $scriptname directory_to_backup"
  echoerr "Exiting."
  exit 1
elif [ $# -eq 1 ]; then
  sourcedir="$1"
fi

# Test sourcedir exists.
if [ ! -d "$sourcedir" ]; then
  echoerr "${scriptname} Error: directory ${sourcedir} not found."
  echoerr "Exiting."
  exit 1
else
  echo "Setting rsnapshot to back up ${sourcedir}"
fi

# Test sourcedir has a trailing / as needed for rsnapshot. If not, add it.
if [ "${sourcedir: -1}" != "/" ]; then
  sourcedir="${sourcedir}/"
fi

# Get a list of mounted disks (apart from sda which is always the internal
# disk on our systems).
# We need to be careful here in case the disk label contains a space.
# This is why we use findmnt, which can give a more explicit listing and
# escape any funny characters.
disks=$($findmnt_bin -n -u -r -o "SOURCE,SIZE,FSTYPE,TARGET" | grep /dev/sd |\
  grep -v /dev/sda)

# Get the number of disks, and list them so the user can choose which to use.
ndisks=$(echo "$disks" | wc -l)
if (( ndisks == 0 )); then
  echoerr "{scriptname} Error: No additional disks found."
  echoerr "Please ensure any attached external drives are mounted."
  echoerr "Exiting."
  exit 1
fi

for idisk in $(seq 1 $ndisks); do
  dline=$(echo -e "$disks" | awk -v l=$idisk 'FNR==l {print}')
  echo ${idisk}: $dline
done

echo "Please enter the number of the disk you would like to use:"
read idisk

if (( idisk < 1 || idisk > ndisks)); then
  echoerr "${scriptname} Error. Invalid disk number. Exiting."
  exit 1
fi

# Check the type and mount point.
dtype=$(echo "$disks" | awk -v l=$idisk 'FNR==l {print $3}')
ddir=$(echo "$disks" | awk -v l=$idisk 'FNR==l {print $4}')
# rsnapshot 1.3.1 may fail when using a directory with a space in it.
# I'm unsure if this is patched in the current debian version.
# Output a warning in this case.
# First get the rsnapshot version:
rsversion=$($rsnapshot_bin --version 2>/dev/null | tail -1 | awk '{print $2}')
hasspace=$(echo "$ddir" | grep "\x20")
# The grep exit code is 0 if the text was found.
if [[ $? == 0 && $rsversion == "1.3.1" ]]; then
  echo "${scriptname} Warning:"
  echo -e "The mounted directory '$ddir' contains spaces."
  echo "This may cause issues with the detected version of rsnapshot."
  echo "The safest option is to edit the usb disk label. This can be done with"
  echo "the gnome-disks utility using the edit partition option."
  echo "Proceed anyway? (y/n)"
  read yorn
  if [[ $yorn != "y" ]]; then
    echo "Exiting."
    exit
  fi
fi

# Convert any hexcodes (spaces in particular) to something usable
ddir=$(echo -e $ddir)

echo "The disk mounted at '$ddir' is formatted as $dtype."
if [[ ! $dtype =~ ^ext.*$ ]]; then
  echo "It may be better to use a disk formatted as ext3 or ext4."
  echo "You could use the gnome-disk utility to format this disk."
  echo "Proceed with $dtype formatted disk anyway? (y/n)"
  read yorn
  if [[ $yorn != "y" ]]; then
    echo "Exiting."
    exit
  fi
fi

conffile="${ddir}/rsnapshot.conf"
logfile="${ddir}/rsnapshot.log"
lockfile="${ddir}/rsnapshot.pid"
cronfile="${ddir}/rsnapshot.crontab"

targetdir="${ddir}/snapshots/"
# If this directory doesn't exist, create it.
if [ ! -d "$targetdir" ]; then
  echo "Creating backup directory '${targetdir}'"
  mkdir "$targetdir"
  if [ $? -ne 0 ]; then
    exit 1
  fi
fi

echo "Generating the config file in '${conffile}'"
touch "${conffile}"
if [ $? -ne 0 ]; then
  exit 1
fi
# We include a full modified copy of the current debian default config here.
# This will make it easier for people to modify this after generation.
cat <<EOF > "${conffile}"
#################################################
# rsnapshot.conf - rsnapshot configuration file #
#################################################
#                                               #
# PLEASE BE AWARE OF THE FOLLOWING RULES:       #
#                                               #
# This file requires tabs between elements      #
#                                               #
# Directories require a trailing slash:         #
#   right: /home/                               #
#   wrong: /home                                #
#                                               #
#################################################

#######################
# CONFIG FILE VERSION #
#######################

config_version	1.2

###########################
# SNAPSHOT ROOT DIRECTORY #
###########################

# All snapshots will be stored under this root directory.
#
snapshot_root	$targetdir

# If no_create_root is enabled, rsnapshot will not automatically create the
# snapshot_root directory. This is particularly useful if you are backing
# up to removable media, such as a FireWire or USB drive.
#
no_create_root	1

#################################
# EXTERNAL PROGRAM DEPENDENCIES #
#################################

# LINUX USERS:   Be sure to uncomment "cmd_cp". This gives you extra features.
# EVERYONE ELSE: Leave "cmd_cp" commented out for compatibility.
#
# See the README file or the man page for more details.
#
cmd_cp		/bin/cp

# uncomment this to use the rm program instead of the built-in perl routine.
#
cmd_rm		/bin/rm

# rsync must be enabled for anything to work. This is the only command that
# must be enabled.
#
cmd_rsync	$rsync_bin

# Uncomment this to enable remote ssh backups over rsync.
#
#cmd_ssh	/usr/bin/ssh

# Comment this out to disable syslog support.
#
cmd_logger	/usr/bin/logger

# Uncomment this to specify the path to "du" for disk usage checks.
# If you have an older version of "du", you may also want to check the
# "du_args" parameter below.
#
cmd_du		$du_bin

# Uncomment this to specify the path to rsnapshot-diff.
#
$rdiff_line

# Specify the path to a script (and any optional arguments) to run right
# before rsnapshot syncs files
#
#cmd_preexec	/path/to/preexec/script

# Specify the path to a script (and any optional arguments) to run right
# after rsnapshot syncs files
#
#cmd_postexec	/path/to/postexec/script

# Paths to lvcreate, lvremove, mount and umount commands, for use with
# Linux LVMs.
#
#linux_lvm_cmd_lvcreate	/sbin/lvcreate
#linux_lvm_cmd_lvremove	/sbin/lvremove
#linux_lvm_cmd_mount	/bin/mount
#linux_lvm_cmd_umount	/bin/umount

#########################################
#           BACKUP INTERVALS            #
# Must be unique and in ascending order #
# i.e. hourly, daily, weekly, etc.      #
#########################################

#retain		hourly	6
retain		daily	7
retain		weekly	4
retain		monthly	3

############################################
#              GLOBAL OPTIONS              #
# All are optional, with sensible defaults #
############################################

# Verbose level, 1 through 5.
# 1     Quiet           Print fatal errors only
# 2     Default         Print errors and warnings only
# 3     Verbose         Show equivalent shell commands being executed
# 4     Extra Verbose   Show extra verbose information
# 5     Debug mode      Everything
#
verbose		2

# Same as "verbose" above, but controls the amount of data sent to the
# logfile, if one is being used. The default is 3.
#
loglevel	3

# If you enable this, data will be written to the file you specify. The
# amount of data written is controlled by the "loglevel" parameter.
#
logfile	$logfile

# If enabled, rsnapshot will write a lockfile to prevent two instances
# from running simultaneously (and messing up the snapshot_root).
# If you enable this, make sure the lockfile directory is not world
# writable. Otherwise anyone can prevent the program from running.
#
lockfile	$lockfile

# By default, rsnapshot check lockfile, check if PID is running
# and if not, consider lockfile as stale, then start
# Enabling this stop rsnapshot if PID in lockfile is not running
#
#stop_on_stale_lockfile		0

# Default rsync args. All rsync commands have at least these options set.
#
#rsync_short_args	-a
#rsync_long_args	--delete --numeric-ids --relative --delete-excluded

# ssh has no args passed by default, but you can specify some here.
#
#ssh_args	-p 22

# Default arguments for the "du" program (for disk space reporting).
# The GNU version of "du" is preferred. See the man page for more details.
# If your version of "du" doesn't support the -h flag, try -k flag instead.
#
#du_args	-csh

# If this is enabled, rsync won't span filesystem partitions within a
# backup point. This essentially passes the -x option to rsync.
# The default is 0 (off).
#
#one_fs		0

# The include and exclude parameters, if enabled, simply get passed directly
# to rsync. If you have multiple include/exclude patterns, put each one on a
# separate line. Please look up the --include and --exclude options in the
# rsync man page for more details on how to specify file name patterns.
#
#include	???
#include	???
#exclude	???
#exclude	???

# The include_file and exclude_file parameters, if enabled, simply get
# passed directly to rsync. Please look up the --include-from and
# --exclude-from options in the rsync man page for more details.
#
#include_file	/path/to/include/file
#exclude_file	/path/to/exclude/file

# If your version of rsync supports --link-dest, consider enable this.
# This is the best way to support special files (FIFOs, etc) cross-platform.
# The default is 0 (off).
#
#link_dest	0

# When sync_first is enabled, it changes the default behaviour of rsnapshot.
# Normally, when rsnapshot is called with its lowest interval
# (i.e.: "rsnapshot hourly"), it will sync files AND rotate the lowest
# intervals. With sync_first enabled, "rsnapshot sync" handles the file sync,
# and all interval calls simply rotate files. See the man page for more
# details. The default is 0 (off).
#
#sync_first	0

# If enabled, rsnapshot will move the oldest directory for each interval
# to [interval_name].delete, then it will remove the lockfile and delete
# that directory just before it exits. The default is 0 (off).
#
#use_lazy_deletes	0

# Number of rsync re-tries. If you experience any network problems or
# network card issues that tend to cause ssh to crap-out with
# "Corrupted MAC on input" errors, for example, set this to a non-zero
# value to have the rsync operation re-tried
#
#rsync_numtries 0

# LVM parameters. Used to backup with creating lvm snapshot before backup
# and removing it after. This should ensure consistency of data in some special
# cases
#
# LVM snapshot(s) size (lvcreate --size option).
#
#linux_lvm_snapshotsize	100M

# Name to be used when creating the LVM logical volume snapshot(s).
#
#linux_lvm_snapshotname	rsnapshot

# Path to the LVM Volume Groups.
#
#linux_lvm_vgpath	/dev

# Mount point to use to temporarily mount the snapshot(s).
#
#linux_lvm_mountpath	/path/to/mount/lvm/snapshot/during/backup

###############################
### BACKUP POINTS / SCRIPTS ###
###############################

backup	$sourcedir		./
# LOCALHOST
#backup	/home/		localhost/
#backup	/etc/		localhost/
#backup	/usr/local/	localhost/
#backup	/var/log/rsnapshot		localhost/
#backup	/etc/passwd	localhost/
#backup	/home/foo/My Documents/		localhost/
#backup	/foo/bar/	localhost/	one_fs=1, rsync_short_args=-urltvpog
#backup_script	/usr/local/bin/backup_pgsql.sh	localhost/postgres/
# You must set linux_lvm_* parameters below before using lvm snapshots
#backup	lvm://vg0/xen-home/	lvm-vg0/xen-home/

# EXAMPLE.COM
#backup_script	/bin/date "+ backup of example.com started at %c"	unused1
#backup	root@example.com:/home/	example.com/	+rsync_long_args=--bwlimit=16,exclude=core
#backup	root@example.com:/etc/	example.com/	exclude=mtab,exclude=core
#backup_script	ssh root@example.com "mysqldump -A > /var/db/dump/mysql.sql"	unused2
#backup	root@example.com:/var/db/dump/	example.com/
#backup_script	/bin/date	"+ backup of example.com ended at %c"	unused9

# CVS.SOURCEFORGE.NET
#backup_script	/usr/local/bin/backup_rsnapshot_cvsroot.sh	rsnapshot.cvs.sourceforge.net/

# RSYNC.SAMBA.ORG
#backup	rsync://rsync.samba.org/rsyncftp/	rsync.samba.org/rsyncftp/

EOF

echo "This has set the rsnapshot log to be saved in '${logfile}'"
echo
echo "Generating a crontab file in '${cronfile}'"
touch "${cronfile}"
if [ $? -ne 0 ]; then
  echoerr "${scriptname} Error: cannot create ${cronfile}."
  exit 1
fi

echo "Please enter the email address you would like to cron errors sent to."
echo "(leave blank to leave unspecified)."
read useremail
if [ -n "$useremail" ]; then
  echo "MAILTO=${useremail}" > ${cronfile}
else
  > ${cronfile}
fi

# In the crontab entries we use the -c flag to rsnapshot to specify the
# config file created above.
cat <<EOF >> "${cronfile}"
# This is a sample cron file for rsnapshot.
# The snapshopt intervals used correspond to the generated rsnapshot.conf.
#
# To activate this cron file manually you can type
# crontab rsnapshot.crontab

# 0 */4		* * *		"${rsnapshot_bin}" -c ${conffile} hourly
30 3  	* * *		"${rsnapshot_bin}" -c "${conffile}" daily
0  3  	* * 1		"${rsnapshot_bin}" -c "${conffile}" weekly
30 2  	1 * *		"${rsnapshot_bin}" -c "${conffile}" monthly
EOF

crondata=$(crontab -l 2>/dev/null)
if [ ! -z "$crondata" ]; then
  echo
  echo "Your crontab currently contains the following:"
  echo "$crondata"
  echo
  echo "Adding rsnapshot entries will set rsnapshot to run automatically at"
  echo "specified intervals."
  echo "Would you like to overwrite your previous entries (o), append to this"
  echo "with entries for rsnapshot (a) or exit now (e)?"
  echo "(o/a/e):"
  read oae
  if [[ $oae == "o" ]]; then
    crontab "${cronfile}"
    if [ $? -eq 0 ]; then
      echo "crontab successfully overwritten."
    fi
  elif [[ $oae == "a" ]]; then
    (crontab -l 2>/dev/null; cat "${cronfile}") | crontab -
    if [ $? -eq 0 ]; then
      echo "crontab successfully updated."
    fi
  fi
else
  echo
  echo "You do not yet have any entries in your crontab."
  echo "Adding rsnapshot entries will set rsnapshot to run automatically at"
  echo "specified intervals."
  echo "Would you like to add rsnapshot entries to your crontab? (y/n)"
  read yorn
  if [[ $yorn == "y" ]]; then
    crontab "${cronfile}"
  fi
fi
echo "${scriptname}: All operations complete."
echo "  Use 'crontab -e' to manually edit your crontab file."
echo "  To check disk usage in future use:"
echo "  rsnapshot -c ${conffile} du"
